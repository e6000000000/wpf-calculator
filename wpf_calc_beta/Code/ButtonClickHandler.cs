﻿using System.Windows.Controls;
using BmMathLib;

namespace wpf_calc_beta.Code
{
    public static class ButtonClickHandler
    {
        private static string ConvertSender(string btnKeyword)
        {
            var mathString = string.Empty;

            #region Ahh

            switch (btnKeyword)
            {
                case ("BtnOne"):
                    mathString = "1";
                    break;
                case ("BtnTwo"):
                    mathString = "2";
                    break;
                case ("BtnThree"):
                    mathString = "3";
                    break;
                case ("BtnFour"):
                    mathString = "4";
                    break;
                case ("BtnFive"):
                    mathString = "5";
                    break;
                case ("BtnSix"):
                    mathString = "6";
                    break;
                case ("BtnSeven"):
                    mathString = "7";
                    break;
                case ("BtnEight"):
                    mathString = "8";
                    break;
                case ("BtnNine"):
                    mathString = "9";
                    break;
                case ("BtnZero"):
                    mathString = "0";
                    break;
                case ("BtnEqual"):
                    mathString = MathStuff.Equal;
                    break;
                case ("BtnPlus"):
                    mathString = MathStuff.Plus;
                    break;
                case ("BtnMinus"):
                    mathString = MathStuff.Minus;
                    break;
                case ("BtnMult"):
                    mathString = MathStuff.Mult;
                    break;
                case ("BtnDivide"):
                    mathString = MathStuff.Div;
                    break;
                case ("BtnDot"):
                    mathString = MathStuff.Dot;
                    break;
                case ("BtnSqrt"):
                    mathString = MathStuff.Root;
                    break;
                case ("BtnFact"):
                    mathString = MathStuff.Fact;
                    break;
                case ("BtnSin"):
                    mathString = MathStuff.Sine;
                    break;
                case ("BtnCos"):
                    mathString = MathStuff.Cosine;
                    break;
                case ("BtnTan"):
                    mathString = MathStuff.Tangent;
                    break;
                case ("BtnCtg"):
                    mathString = MathStuff.Cotangent;
                    break;
                case ("BtnDeg"):
                    mathString = MathStuff.Deg;
                    break;
                case ("BtnLogComma"):
                    mathString = MathStuff.Comma;
                    break;
                case ("BtnLog"):
                    mathString = MathStuff.Log;
                    break;
                case ("BtnLg"):
                    mathString = MathStuff.Lg;
                    break;
                case ("BtnLn"):
                    mathString = MathStuff.Ln;
                    break;
                case ("BtnPi"):
                    mathString = MathStuff.Pi;
                    break;
                case ("BtnExp"):
                    mathString = MathStuff.Exp;
                    break;
                case ("Pow"):
                    mathString = MathStuff.Pow;
                    break;
                case ("OpenBr"):
                    mathString = "(";
                    break;
                case ("CloseBr"):
                    mathString = ")";
                    break;
                case ("FuncShow"):
                    mathString = "funcsmenu";
                    break;
                case ("Remove"):
                    mathString = "r";
                    break;
                case ("Clear"):
                    mathString = "c";
                    break;
            }

            #endregion

            return mathString;
        }

        public static string HandleSender(object sender)
        {
            var senderName = ((Button) sender).Name;
            return ConvertSender(senderName);
        }
    }
}